# HODOR
import numpy as np


class Hodor:
    """
    Hodor
    """
    def hodor(self, *args, **kwargs):
        """
        Hodor
        """
        return self
    def __repr__(self, *args, **kwargs):
        """
        Hodor
        """
        hodor = [" !", " ?", "...", "", "?!"]
        return "Hodor" + hodor[np.random.randint(len(hodor))]
    
    __getattr__ = hodor
    __getitem__ = hodor
    __call__ = hodor
    __mul__ = hodor
    __rmul__ = hodor
    __add__ = hodor
    __radd__ = hodor
    __pow__ = hodor
    
if __name__ == '__main__':
    hodor = Hodor()
    print(hodor())
