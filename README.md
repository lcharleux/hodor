# Hodor

Hodor !

 
```python
>>>  hodor.aaa()                                                             
'Hodor...'
>>>  hodor.bbb                                                              
Hodor
>>> 3 * hodor()                                                            
'Hodor...Hodor...Hodor...'
>>> 3 * hodor                                                              
'Hodor...'
>>> hodor**4                                                               
'Hodor !'
>>> hodor.h.o.d.o.r()()().r.o.d.o.h
'Hodor...'
>>> hodor["hodor"]  
'Hodor ?!'
```
